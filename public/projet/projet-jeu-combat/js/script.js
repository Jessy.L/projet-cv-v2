// Var de start

var set_game = 0;

var regles = document.getElementById('regles').addEventListener('click', popup)


var persoJoueur = "";
var vie_perso = "";

var persoBot = "";
var vie_bot = "";

var fin_parti_lose = 0

var fin_parti_win = 0
// var de jeu 

var personnage = document.getElementById('choix-perso');

var nb_combat_gagner = document.getElementById('combat_gagner')
var nb_combat_perdu = document.getElementById('combat_perdu')

var roi_option = document.getElementById('roi')
var ange_option = document.getElementById('ange')
var chevalier_option = document.getElementById('chevalier')
var reine_option = document.getElementById('reine')

var image_fond = document.getElementById('fond-ecran')

var btn = document.getElementById('btn').addEventListener('click', stat);

var arene = document.getElementById('arene').addEventListener('click', validstats);

var combat_button = document.getElementById('combat_button').addEventListener('click', value_combat);

var recommencer = document.getElementById('recommencer')
recommencer.addEventListener('click', reset_game)

var attack_list = document.getElementById('attack-list')
attack_list.style.display = "none"

var combat_actu = document.getElementById('combat-en-cours')
var vie_perso_text = document.getElementById("vie_perso")
var vie_bot_text = document.getElementById('vie_bot')

var historique = document.getElementById('historique')

var img_perso = document.getElementById('img_perso')
var img_bot = document.getElementById('img_bot')

var nom_bot = document.getElementById('nom_bot')

var atk1b = document.getElementById('atk1b')
var atk2b = document.getElementById('atk1b')
var atk3b = document.getElementById('atk1b')

var attack1 = document.createElement('option')
var attack2 = document.createElement('option')
var attack3 = document.createElement('option')

attack_list.appendChild(attack1)
attack_list.appendChild(attack2)
attack_list.appendChild(attack3)


var attack1b = document.createElement('option')
var attack2b = document.createElement('option')
var attack3b = document.createElement('option')


// function de start


document.addEventListener('DOMContentLoaded', function setgame(){
    if(set_game == 0){
        console.log('jeu set')
        img_perso.src = 'img/'+ personnage.value + '.jpg'
        set_game = 1 
        recommencer.style.display = 'none'
        return set_game
        
    }else if(set_game == 1){
        console.log('Le jeu est dejà set')
    } else {
        console.error('ERROR SET_GAME > 1')
    }
});

function popup(){
    window.open("regles.html", "Régles", "height=500px, width=900px, menubar='no', toolbar='no', location='no', status='no', scrollbars='no'");
}

function reset_game(){

    roi_option.style.display = 'visible'
    ange_option.style.display = 'visible'
    chevalier_option.style.display = 'visible'
    reine_option.style.display = 'visible'
    recommencer.style.display = 'none'


    historique.innerText = ""

    combat_actu.innerHTML = "Le combat n'implique aucun personnages pour le moment "
    vie_perso_text.innerHTML = 'La vie du personnage : 0'
    vie_bot_text.innerHTML = 'La vie du bot : 0' 
}


//Function du jeu 

// ______________________________________________function stat __________________________________________________________________________________


function stat(){

    attack_list.style.display = ""

    if(personnage.value == 'ange'){

        let ange = {
            'nom' : 'Ange',
            'vie' : 150,
            'xp' : 0,
            
        }
        
        attack1.innerText = "Sphère célèste."  
        attack1.value = 10
        attack2.innerText = "Lance de l'ange."
        attack2.value = 7
        attack3.innerText = "Ailes Spirituelles"
        attack3.value = 20

        img_perso.src = 'img/ange.jpg'

        persoJoueur = ange

        aleatoire(persoJoueur)


        return persoJoueur

    } else if(personnage.value == 'chevalier'){
        let chevalier = {
            'nom' :'Chevalier',
            'vie' : 170,
            'xp' : 0,
        
        }

        attack1.innerText = "Epée sacrée."
        attack1.value = 10 
        attack2.innerText = "Lumière sacrée."
        attack2.value = 14
        attack3.innerText = "Appel Spirituelles"
        attack3.value = 17

        img_perso.src = 'img/chevalier.jpg'

        persoJoueur = chevalier

        aleatoire(persoJoueur)


        return persoJoueur

    } else if(personnage.value == 'roi'){
        let roi = {
            'nom' : 'Roi',
            'vie' : 250,
            'xp' : 0,
        
        }

        attack1.innerText = "Lumière du throne."
        attack1.value = 17
        attack2.innerText = "Bouclier des dieux."
        attack2.value = 15
        attack3.innerText = "Feu bleu"
        attack3.value = 13

        img_perso.src = 'img/roi.jpg'

        persoJoueur = roi

        aleatoire(persoJoueur)


        return persoJoueur

    } else if(personnage.value == 'reine'){

        let reine = {
            'nom' : 'Reine',
            'vie' : 100,
            'xp' : 0,
        
        }

        attack1.innerText = "Epée du roi."
        attack1.value  = 5
        attack2.innerText = "Lance des dieux."
        attack2.value = 16
        attack3.innerText = "Ailes des anges"
        attack3.value = 13

        img_perso.src = 'img/reine.jpg'

        persoJoueur = reine

        aleatoire(persoJoueur)

        return persoJoueur

    } else if(personnage.value == 'dieu'){
        let dieu = {
            'nom' : 'Dieu',
            'vie' : 100000,
            'xp' : 0,
        
        
        }

        attack1.innerText = "Ultime attaque."
        attack1.value = 10000
        attack2.innerText = "Eclair de Zeus."
        attack2.value = 100000
        attack3.innerText = "Chasse ténébre"
        attack3.value = 1000

        img_perso.src = 'img/dieu.jpg'


        persoJoueur = dieu

        aleatoire(persoJoueur)

        return persoJoueur

        
    } else {
        attack_list.style.display = "none"
    }

    
}


// ______________________________________________function aleatoire __________________________________________________________________________________


function aleatoire(persoJoueur){
    var chiffre = Math.floor(Math.random() * Math.floor(5))
    // console.log(chiffre)
    bot(persoJoueur,chiffre)
}

// ______________________________________________function bot __________________________________________________________________________________


function bot(persoJoueur,chiffre){
    if(chiffre == 0 ){

        let sbire = {
            'nom' : 'Sbire',
            'vie' : 100,
            'xp' : 0,
            'atk1' : 5,
            'atk2' : 9,
            'atk3' : 12,
        }

        attack1b.value = 5;
        attack2b.value = 9;
        attack3b.value = 12;

        image_fond.src = 'img/chateau.jpg'
        img_bot.src = 'img/sbire.png'

        persoBot = sbire;

        return persoJoueur, persoBot;

    } else if(chiffre == 1){
        let demon = {
            'nom' : 'Demon',
            'vie' : 120,
            'xp' : 0,
            'atk1' : 9,
            'atk2' : 17,
            'atk3' : 24,
        }

        attack1b.value = 9;
        attack2b.value = 17;
        attack3b.value = 24;

        image_fond.src = 'img/temple.jpg'
        img_bot.src = 'img/demon.jpg'

        persoBot = demon

        return persoJoueur, persoBot

    } else if(chiffre == 2){
        let troupe = {
            'nom' : 'Troupe de démons',
            'vie' : 140,
            'xp' : 0,
            'atk1' : 10,
            'atk2' : 18,
            'atk3' : 24,
        }

        attack1b.value = 10;
        attack2b.value = 18;
        attack3b.value = 24;

        image_fond.src = 'img/jungle.jpg'
        img_bot.src = 'img/troupe.png'

        persoBot = troupe

        return persoJoueur, persoBot

    } else if(chiffre == 3){
        let reine_de_la_mort = {
            'nom' : 'Reine de la mort',
            'vie' : 180,
            'xp' : 0,
            'atk1' : 20,
            'atk2' : 47,
            'atk3' : 100,
        }

        attack1b.value = 20;
        attack2b.value = 47;
        attack3b.value = 100;

        image_fond.src = 'img/hell.jpg'
        img_bot.src = 'img/reine-de-la-mort.jpg'

        persoBot = reine_de_la_mort

        return persoJoueur, persoBot

    } else if(chiffre == 4){
        let diable = {
            'nom' : 'Diable',
            'vie' : 300,
            'xp' : 0,
            'atk1' : 50,
            'atk2' : 74,
            'atk3' : 101,
        }

        attack1b.value = 50;
        attack2b.value = 74;
        attack3b.value = 101;

        image_fond.src = 'img/hell.jpg'
        img_bot.src = 'img/diable.jpg'

        persoBot = diable

        return persoJoueur, persoBot
    
    } else{
        console.error("Function bot no selection")
    }

    return persoJoueur, persoBot
}

// ______________________________________________function combat __________________________________________________________________________________


function validstats(){
    vie_perso = persoJoueur.vie
    vie_bot = persoBot.vie

    console.log("validstats "+ vie_perso)
    console.log("validstats "+ vie_bot)

    return vie_perso , vie_bot
}



//_______________________________________________Function value_combat_____________________________________________________________________________


function value_combat(){

    combat_actu.innerHTML = 'Le combat implique ' + persoJoueur.nom + ' et ' + persoBot.nom
    vie_perso_text.innerHTML = 'La vie du personnage : ' + vie_perso
    vie_bot_text.innerHTML = 'La vie du bot : ' + vie_bot  
    nom_bot.innerHTML = "L'adversaire est " + persoBot.nom

    combat(vie_perso,vie_bot)

}


function combat(vie_perso,vie_bot){

    console.log('combat function')
    var li = document.createElement('li')
    // li.innerHTML = 'test'
    historique.appendChild(li)

    // Choix attaque du bot 

    var atkb_choix = Math.floor(Math.random() * Math.floor(3))
    if(vie_bot <= 0){

        console.log('le bot est mort')

        fin_parti_win = fin_parti_win + 1

        nb_combat_gagner.innerText = 'Victoire : ' + fin_parti_win


        if(fin_parti_win == 4){
            recommencer.style.display = ''
            alert('Vous avez Gagner')

        }else{
            console.log('Le combat continue')
        }
        return fin_parti_win

    }else{
        if(atkb_choix == 0){

            vie_perso = vie_perso - Number(attack1b.value)
            console.log('vie bot = ' + vie_perso)
    
            document.getElementById('vie_perso').dataset.viep = vie_perso
            document.getElementById('vie_perso').innerText = 'La vie du joueur : ' + document.getElementById('vie_perso').dataset.viep
           
            li.innerHTML = 'Le bot lance une attaque de ' + Number(attack1b.value) + ' de dégats'
            historique.insertAdjacentElement('afterbegin', li)

            
        
            combatpart2(vie_perso,vie_bot)
            
            
        }else if(atkb_choix == 1){

            vie_perso = vie_perso - Number(attack2b.value)
            console.log('vie bot = ' + vie_perso)
    
            document.getElementById('vie_perso').dataset.viep = vie_perso
            document.getElementById('vie_perso').innerText = 'La vie du joueur : ' + document.getElementById('vie_perso').dataset.viep
           
            li.innerHTML = 'Le bot lance une attaque de ' + Number(attack2b.value) + ' de dégats'
            historique.insertAdjacentElement('afterbegin', li)

            
        
            combatpart2(vie_perso,vie_bot)
            

        }else if(atkb_choix == 2){
            
            vie_perso = vie_perso - Number(attack3b.value)
            console.log('vie bot = ' + vie_perso)
    
            document.getElementById('vie_perso').dataset.viep = vie_perso
            document.getElementById('vie_perso').innerText = 'La vie du joueur : ' + document.getElementById('vie_perso').dataset.viep
           
            li.innerHTML = 'Le bot lance une attaque de ' + Number(attack3b.value) + ' de dégats'
            historique.insertAdjacentElement('afterbegin', li)

            
        
            combatpart2(vie_perso,vie_bot)


        }else{
            console.error("Attack BOT No selected")
        }
    }
}


function combatpart2(vie_perso,vie_bot){

    console.log("combatP2 VP "+ vie_perso)
    console.log("combatP2 VB "+ vie_bot)
    console.log("attact ")
    var li = document.createElement('li')


    // return vie_perso,vie_bot,attackBot

    if(vie_perso <= 0 ){
        console.log('le joueur est mort')

        var chaine = persoJoueur.nom + '_option'
        var mort = eval(chaine.toLowerCase())
        mort.innerHTML = ""
        
        fin_parti_lose = fin_parti_lose + 1

        nb_combat_perdu.innerText = 'Défaite : ' + fin_parti_lose


        console.log(mort)
        mort.style.display = "none"

        if(fin_parti_lose == 4){
            recommencer.style.display = ''
            alert('Vous avez perdu')

        }else{
            console.log('Le combat continue')
        }
        return fin_parti_lose
    }else{
        vie_bot = vie_bot - Number(attack_list.value)
        console.log('vie bot = ' + vie_bot)

        document.getElementById('vie_bot').dataset.vieb = vie_bot
        document.getElementById('vie_bot').innerText = 'La vie du Bot : ' + document.getElementById('vie_bot').dataset.vieb
       
        li.innerHTML = 'Le joueur lance une attaque de ' + Number(attack_list.value) + ' de dégats'
        historique.insertAdjacentElement('afterbegin', li)

        combat(vie_perso,vie_bot) 
    }

    return vie_perso,vie_bot,fin_parti_lose
}
