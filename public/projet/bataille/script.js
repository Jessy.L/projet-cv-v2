var carte_button = document.getElementById('carte_button').addEventListener('click', selection) 

var carte_equal_joueur = document.getElementById('joueur')
carte_equal_joueur.style.display = 'none'

var carte_equal_bot = document.getElementById('bot')
carte_equal_bot.style.display = 'none'


var img_html_joueur = document.getElementById('img_html_joueur')
var img_html_bot = document.getElementById('img_html_bot')

var compte_carte_joueur = document.getElementById('compte_carte_joueur')
var compte_carte_bot = document.getElementById('compte_carte_bot')



var carte_noir = [
    
    // Cartes Trefle
    
    "clubs_ace.png" ,
    "clubs_2.png",
    "clubs_3.png",
    "clubs_4.png",
    "clubs_5.png",
    "clubs_6.png",
    "clubs_7.png",
    "clubs_8.png",
    "clubs_9.png",
    "clubs_10.png" ,
    "clubs_jack.png" ,
    "clubs_queen.png",
    "clubs_king.png" ,
    
    // Carte Pique
    
    "spade_ace.png" ,
    "spade_2.png",
    "spade_3.png",
    "spade_4.png",
    "spade_5.png",
    "spade_6.png",
    "spade_7.png",
    "spade_8.png",
    "spade_9.png",
    "spade_10.png" ,
    "spade_jack.png" ,
    "spade_queen.png",
    "spade_king.png"
    
]


var carte_rouge = [
    
    // Cartes Coeur
    
    "heart_ace.png" ,
    "heart_2.png",
    "heart_3.png",
    "heart_4.png",
    "heart_5.png",
    "heart_6.png",
    "heart_7.png",
    "heart_8.png",
    "heart_9.png",
    "heart_10.png" ,
    "heart_jack.png" ,
    "heart_queen.png",
    "heart_king.png",
    
    // Cartes Diamant
    
    "diamond_ace.png" ,
    "diamond_2.png",
    "diamond_3.png",
    "diamond_4.png",
    "diamond_5.png",
    "diamond_6.png",
    "diamond_7.png",
    "diamond_8.png",
    "diamond_9.png",
    "diamond_10.png" ,
    "diamond_jack.png" ,
    "diamond_queen.png",
    "diamond_king.png"
]


let img_carte = {
    
    // Cartes Treffe
    
    "clubs_ace.png" : 14,
    "clubs_2.png" : 2,
    "clubs_3.png" : 3,
    "clubs_4.png" : 4,
    "clubs_5.png" : 5,
    "clubs_6.png" : 6,
    "clubs_7.png" : 7,
    "clubs_8.png" : 8,
    "clubs_9.png" : 9,
    "clubs_10.png" : 10,
    "clubs_jack.png" : 11,
    "clubs_queen.png" : 12,
    "clubs_king.png" : 13,
    
    // Cartes PIC
    
    "spade_ace.png" : 14,
    "spade_2.png" : 2,
    "spade_3.png" : 3,
    "spade_4.png" : 4,
    "spade_5.png" : 5,
    "spade_6.png" : 6,
    "spade_7.png" : 7,
    "spade_8.png" : 8,
    "spade_9.png" : 9,
    "spade_10.png" : 10,
    "spade_jack.png" : 11,
    "spade_queen.png" : 12,
    "spade_king.png" : 13,
    
    // Cartes Diamant
    
    "diamond_ace.png" : 14,
    "diamond_2.png" : 2,
    "diamond_3.png" : 3,
    "diamond_4.png" : 4,
    "diamond_5.png" : 5,
    "diamond_6.png" : 6,
    "diamond_7.png" : 7,
    "diamond_8.png" : 8,
    "diamond_9.png" : 9,
    "diamond_10.png" : 10,
    "diamond_jack.png" : 11,
    "diamond_queen.png" : 12,
    "diamond_king.png" : 13,
    
    // Cartes Coeur
    
    "heart_ace.png" : 14,
    "heart_2.png" : 2,
    "heart_3.png" : 3,
    "heart_4.png" : 4,
    "heart_5.png" : 5,
    "heart_6.png" : 6,
    "heart_7.png" : 7,
    "heart_8.png" : 8,
    "heart_9.png" : 9,
    "heart_10.png" : 10,
    "heart_jack.png" : 11,
    "heart_queen.png" : 12,
    "heart_king.png" : 13,
}

var chiffre_joueur = Number(carte_noir.length);
var chiffre_bot = Number(carte_rouge.length);


var stock_carte_joueur = ""
var stock_carte_bot = ""

function selection(){
    
    chiffre_joueur = Number(carte_noir.length);
    chiffre_bot = Number(carte_rouge.length);
    

    compte_carte_joueur.innerHTML = "Nombre de carte du joueur : " +  chiffre_joueur
    compte_carte_bot.innerHTML = "Nombre de carte du bot : " +  chiffre_bot


    carte_equal_joueur.innerHTML = ""
    carte_equal_joueur.style.display = "none"

    carte_equal_bot.innerHTML = ""
    carte_equal_bot.style.display = "none"
    
    var carte_noir_aleatoire = Math.floor(Math.random() * Math.floor(chiffre_joueur))
    var carte_rouge_aleatoire = Math.floor(Math.random() * Math.floor(chiffre_bot))
    
    console.log('carte_noir_aleatoire ' + carte_noir_aleatoire)
    console.log('carte_rouge_aleatoire ' + carte_rouge_aleatoire)
    
    stock_carte_joueur = img_carte[carte_noir[carte_noir_aleatoire]]
    stock_carte_bot = img_carte[carte_rouge[carte_rouge_aleatoire]]
    
    img_html_joueur.src = 'img/' + carte_noir[carte_noir_aleatoire]
    img_html_bot.src = 'img/' + carte_rouge[carte_rouge_aleatoire]

    var img_carte_joueur = carte_noir[carte_noir_aleatoire]
    var img_carte_bot = carte_rouge[carte_rouge_aleatoire]

    console.log(stock_carte_joueur)
    console.log(stock_carte_bot)

    combat(stock_carte_joueur,stock_carte_bot,img_carte_joueur,img_carte_bot,carte_noir_aleatoire,carte_rouge_aleatoire,chiffre_joueur,chiffre_bot)
}

function combat(stock_carte_joueur,stock_carte_bot,img_carte_joueur,img_carte_bot,carte_noir_aleatoire,carte_rouge_aleatoire,chiffre_joueur,chiffre_bot){
    if(stock_carte_joueur > stock_carte_bot){

        // Change le tableau des cartes 
        carte_noir.push(img_carte_bot)
        carte_rouge.splice(carte_rouge_aleatoire, 1)

        // change la taille des chiffre aléatoire
        chiffre_joueur = Number(carte_noir.length);
        chiffre_bot = Number(carte_rouge.length);


        if(chiffre_bot == 0){
            alert("Vous avez gagner")
        }else if(chiffre_joueur == 0 ){
            alert('Vous avez perdu')
        }else{
            
            // return le tout 
            return carte_rouge , carte_noir, chiffre_joueur, chiffre_bot

        }

    }else if(stock_carte_joueur < stock_carte_bot){
        
        // Change le tableau des cartes 
        carte_rouge.push(img_carte_joueur)
        carte_noir.splice(carte_noir_aleatoire, 1)

        // change la taille des chiffre aléatoire
        chiffre_joueur = Number(carte_noir.length);
        chiffre_bot = Number(carte_rouge.length);        
        
        if(chiffre_bot == 0){
            alert("Vous avez gagner")
        }else if(chiffre_joueur == 0 ){
            alert('Vous avez perdu')
        }else{
            // return le tout 
            return carte_rouge , carte_noir, chiffre_joueur, chiffre_bot
        }

    }else if( stock_carte_joueur == stock_carte_bot){
        console.log("EQUAL")

        carte_equal_bot.style.display = ''
        carte_equal_joueur.style.display = ''

        var carte_noir_aleatoire_equal = Math.floor(Math.random() * Math.floor(chiffre_joueur))
        var carte_rouge_aleatoire_equal = Math.floor(Math.random() * Math.floor(chiffre_bot))

        var img_equal_1 =  document.createElement('img')
        var img_equal_2 = document.createElement('img')

        carte_equal_joueur.appendChild(img_equal_1)
        carte_equal_bot.appendChild(img_equal_2)

        stock_carte_joueur_equal = img_carte[carte_noir[carte_noir_aleatoire_equal]]
        stock_carte_bot_equal = img_carte[carte_rouge[carte_rouge_aleatoire_equal]]
        
        img_equal_1.src = 'img/' + carte_noir[carte_noir_aleatoire_equal]
        img_equal_2.src = 'img/' + carte_rouge[carte_rouge_aleatoire_equal]

        var img_carte_joueur = carte_noir[carte_noir_aleatoire_equal]
        var img_carte_bot = carte_rouge[carte_rouge_aleatoire_equal]

        var img_carte_joueur_equal = carte_noir[carte_noir_aleatoire_equal]
        var img_carte_bot_equal = carte_rouge[carte_rouge_aleatoire_equal]

        if(stock_carte_bot_equal < stock_carte_joueur_equal){

            console.log('joueur win equal')

            carte_noir.push(img_carte_bot)
            carte_noir.push(img_carte_bot_equal)

            carte_rouge.splice(carte_rouge_aleatoire, 1)
            carte_rouge.splice(carte_rouge_aleatoire_equal, 1)

            // change la taille des chiffre aléatoire

            chiffre_joueur = Number(carte_noir.length);
            chiffre_bot = Number(carte_rouge.length);

            if(chiffre_bot == 0){
                alert("Vous avez gagner")
            }else if(chiffre_joueur == 0 ){
                alert('Vous avez perdu')
            }else{
                
                // return le tout 
                return carte_rouge , carte_noir, chiffre_joueur, chiffre_bot
    
            }
        }else if(stock_carte_bot_equal > stock_carte_joueur_equal){

            console.log('BOT WIN EQUAL')

            carte_rouge.push(img_carte_joueur)
            carte_rouge.push(img_carte_joueur_equal)

            carte_noir.splice(carte_noir_aleatoire, 1)
            carte_noir.splice(carte_noir_aleatoire_equal, 1)


            // change la taille des chiffre aléatoire
            chiffre_joueur = Number(carte_noir.length);
            chiffre_bot = Number(carte_rouge.length);


            if(chiffre_bot == 0){
                alert("Vous avez gagner")
            }else if(chiffre_joueur == 0 ){
                alert('Vous avez perdu')
            }else{
                
                // return le tout 
                return carte_rouge , carte_noir, chiffre_joueur, chiffre_bot
    
            }

        }else if(stock_carte_bot_equal == stock_carte_joueur_equal){
            
            carte_equal_joueur.innerHTML = ""
            carte_equal_joueur.style.display = "none"
        
            carte_equal_bot.innerHTML = ""
            carte_equal_bot.style.display = "none"

        } else{
            console.error('BOT AND PLAYER EQUAL FUNCTION BUG ')
        }

    }else{
        console.error('Carte ROUGE ' + carte_rouge)
        console.error('Carte Noir ' + carte_noir)

        console.error('Stock var bot ' + stock_carte_bot)
        console.error('Stock var joueur ' + stock_carte_joueur)

        console.error('img carte bot ' + img_carte_bot)
        console.error('img carte joueur ' + img_carte_joueur)

        console.error('random chiffre bot ' + carte_rouge_aleatoire)
        console.error('random chiffre joueur ' + carte_noir_aleatoire)

        console.error('chiffre bot ' + chiffre_bot )
        console.error('chiffre joueur ' + chiffre_joueur )

        console.error("ERROR SELECTIONC CARTE")
    }
}

// function efface(){
//     carte_equal_joueur.innerHTML = ""
//     carte_equal_joueur.style.display = "none"

//     carte_equal_bot.innerHTML = ""
//     carte_equal_bot.style.display = "none"

// }