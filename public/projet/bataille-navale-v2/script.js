var grille = document.getElementById('grille')
var grille_bot = document.getElementById('grille_bot')
var option = document.getElementsByClassName('option')
var div_pre_game = document.getElementById("div_pre-game")

var hor = document.getElementById('hor')
var ver = document.getElementById('ver')
var type = document.getElementById('type')

var setGame_button = document.getElementById('setGame').addEventListener('click', setGame)
var button_lancer = document.getElementById('lancer').addEventListener('click', start)



// function generation de la grille 

function setGame(){
    grille.innerHTML = ""
    grille_bot.innerHTML = ""
    div_pre_game.style.display = ''
    grille.style.display = 'none'


    for(i = 0; i < 100 ; i++){
        div_case = document.createElement('div')
        div_case.addEventListener('click', verif)
        div_case.dataset.nbcase = i + 1
        div_case.className = 'case'
        grille.appendChild(div_case)   
   
    }   

    for(i = 0; i < 100 ; i++){
        div_case_bot = document.createElement('div')
        div_case_bot.addEventListener('click', selection_joueur)
        div_case_bot.dataset.nbcase_bot = i + 1
        div_case_bot.className = 'case_bot'
        grille_bot.appendChild(div_case_bot)   
    }   


    selection_bot() 
    
}


function start(){
    div_pre_game.style.display = 'none'    
    grille.style.display = ''
    selection_bot()
}

function attaque_bot(){
    var atk_bot = Math.floor(Math.random() * Math.floor(101))
    var div_bot = document.getElementsByClassName('case_bot')

    if(div_bot[atk_bot].dataset.selectJoueur == 'tir'){
        attaque_bot()
    }else if(div_bot[atk_bot].dataset.selectJoueur == 'joueur'){
        div_bot[atk_bot].style.backgroundColor = 'Black'
        div_bot[atk_bot].dataset.selectJoueur = 'tir'
    }else{
        div_bot[atk_bot].style.backgroundColor = 'White'
        div_bot[atk_bot].dataset.selectJoueur = 'tir'
    }

}

function selection_joueur(){

    var div_bot = document.getElementsByClassName('case_bot')

    if(type.value == 'barque1' ){


        if(hor.checked == true && ver.checked == false){

            select_bat = this.dataset.nbcase_bot - 1

            var selection_bot = div_bot[select_bat]
            selection_bot.dataset.selectJoueur = "joueur"
            selection_bot.style.backgroundColor = 'green'
            selection_bot_case2 = div_bot[Number(select_bat) + 1] 
            selection_bot_case2.dataset.selectJoueur = "joueur"
            selection_bot_case2.style.backgroundColor = 'green'
            
            option[0].innerHTML = ""
            option[0].value = ""
            
        }else if(hor.checked == false && ver.checked == true){

            
            select_bat = this.dataset.nbcase_bot - 1

            var selection_bot = div_bot[select_bat]
            selection_bot.dataset.selectJoueur = "joueur"
            selection_bot.style.backgroundColor = 'green'
            selection_bot_case2 = div_bot[Number(select_bat) + 10] 
            selection_bot_case2.dataset.selectJoueur = "joueur"
            selection_bot_case2.style.backgroundColor = 'green'
            
            option[0].innerHTML = ""
            option[0].value = ""
        }else{
            console.error('BUG SELECT')
        }
    }else if(type.value == 'barque2' ){

        if(hor.checked == true && ver.checked == false){

            select_bat = this.dataset.nbcase_bot - 1

            var selection_bot = div_bot[select_bat]
            selection_bot.dataset.selectJoueur = "joueur"
            selection_bot.style.backgroundColor = 'green'
            selection_bot_case2 = div_bot[Number(select_bat) + 1] 
            selection_bot_case2.dataset.selectJoueur = "joueur"
            selection_bot_case2.style.backgroundColor = 'green'
            
            option[1].innerHTML = ""
            option[1].value = ""
            
        }else if(hor.checked == false && ver.checked == true){

            
            select_bat = this.dataset.nbcase_bot - 1

            var selection_bot = div_bot[select_bat]
            selection_bot.dataset.selectJoueur = "joueur"
            selection_bot.style.backgroundColor = 'green'
            selection_bot_case2 = div_bot[Number(select_bat) + 10] 
            selection_bot_case2.dataset.selectJoueur = "joueur"
            selection_bot_case2.style.backgroundColor = 'green'
            
            option[1].innerHTML = ""
            option[1].value = ""

        }else{
            console.error('BUG SELECT')
        }
    }else if(type.value == 'navire1' ){

        if(hor.checked == true && ver.checked == false){

            select_bat = this.dataset.nbcase_bot - 1

            var selection_bot = div_bot[select_bat]
            selection_bot.dataset.selectJoueur = "joueur"
            selection_bot.style.backgroundColor = 'green'
            selection_bot_case2 = div_bot[Number(select_bat) + 1] 
            selection_bot_case2.dataset.selectJoueur = "joueur"
            selection_bot_case2.style.backgroundColor = 'green'
            selection_bot_case3 = div_bot[Number(select_bat) + 2] 
            selection_bot_case3.dataset.selectJoueur = "joueur"
            selection_bot_case3.style.backgroundColor = 'green'

            
            option[2].innerHTML = ""
            option[2].value = ""
            
        }else if(hor.checked == false && ver.checked == true){

            
            select_bat = this.dataset.nbcase_bot - 1

            var selection_bot = div_bot[select_bat]
            selection_bot.dataset.selectJoueur = "joueur"
            selection_bot.style.backgroundColor = 'green'
            selection_bot_case2 = div_bot[Number(select_bat) + 10] 
            selection_bot_case2.dataset.selectJoueur = "joueur"
            selection_bot_case2.style.backgroundColor = 'green'
            selection_bot_case3 = div_bot[Number(select_bat) + 20] 
            selection_bot_case3.dataset.selectJoueur = "joueur"
            selection_bot_case3.style.backgroundColor = 'green'
            
            option[2].innerHTML = ""
            option[2].value = ""

        }else{
            console.error('BUG SELECT')
        }
    }else if(type.value == 'navire2' ){

        if(hor.checked == true && ver.checked == false){

            select_bat = this.dataset.nbcase_bot - 1

            var selection_bot = div_bot[select_bat]
            selection_bot.dataset.selectJoueur = "joueur"
            selection_bot.style.backgroundColor = 'green'
            selection_bot_case2 = div_bot[Number(select_bat) + 1] 
            selection_bot_case2.dataset.selectJoueur = "joueur"
            selection_bot_case2.style.backgroundColor = 'green'
            selection_bot_case3 = div_bot[Number(select_bat) + 2] 
            selection_bot_case3.dataset.selectJoueur = "joueur"
            selection_bot_case3.style.backgroundColor = 'green'

            
            option[3].innerHTML = ""
            option[3].value = ""
            
        }else if(hor.checked == false && ver.checked == true){

            
            select_bat = this.dataset.nbcase_bot - 1

            var selection_bot = div_bot[select_bat]
            selection_bot.dataset.selectJoueur = "joueur"
            selection_bot.style.backgroundColor = 'green'
            selection_bot_case2 = div_bot[Number(select_bat) + 10] 
            selection_bot_case2.dataset.selectJoueur = "joueur"
            selection_bot_case2.style.backgroundColor = 'green'
            selection_bot_case3 = div_bot[Number(select_bat) + 20] 
            selection_bot_case3.dataset.selectJoueur = "joueur"
            selection_bot_case3.style.backgroundColor = 'green'
            
            option[3].innerHTML = ""
            option[3].value = ""

        }else{
            console.error('BUG SELECT')
        }
    }else if(type.value == 'cuirasse' ){

        if(hor.checked == true && ver.checked == false){

            select_bat = this.dataset.nbcase_bot - 1

            var selection_bot = div_bot[select_bat]
            selection_bot.dataset.selectJoueur = "joueur"
            selection_bot.style.backgroundColor = 'green'
            selection_bot_case2 = div_bot[Number(select_bat) + 1] 
            selection_bot_case2.dataset.selectJoueur = "joueur"
            selection_bot_case2.style.backgroundColor = 'green'
            selection_bot_case3 = div_bot[Number(select_bat) + 2] 
            selection_bot_case3.dataset.selectJoueur = "joueur"
            selection_bot_case3.style.backgroundColor = 'green'
            selection_bot_case4 = div_bot[Number(select_bat) + 3] 
            selection_bot_case4.dataset.selectJoueur = "joueur"
            selection_bot_case4.style.backgroundColor = 'green'


            
            option[4].innerHTML = ""
            option[4].value = ""
            
        }else if(hor.checked == false && ver.checked == true){

            
            select_bat = this.dataset.nbcase_bot - 1

            var selection_bot = div_bot[select_bat]
            selection_bot.dataset.selectJoueur = "joueur"
            selection_bot.style.backgroundColor = 'green'
            selection_bot_case2 = div_bot[Number(select_bat) + 10] 
            selection_bot_case2.dataset.selectJoueur = "joueur"
            selection_bot_case2.style.backgroundColor = 'green'
            selection_bot_case3 = div_bot[Number(select_bat) + 20] 
            selection_bot_case3.dataset.selectJoueur = "joueur"
            selection_bot_case3.style.backgroundColor = 'green'
            selection_bot_case4 = div_bot[Number(select_bat) + 30] 
            selection_bot_case4.dataset.selectJoueur = "joueur"
            selection_bot_case4.style.backgroundColor = 'green'

            
            option[4].innerHTML = ""
            option[4].value = ""

        }else{
            console.error('BUG SELECT')
        }
    }else if(type.value == 'porte-avion' ){

        if(hor.checked == true && ver.checked == false){

            select_bat = this.dataset.nbcase_bot - 1

            var selection_bot = div_bot[select_bat]
            selection_bot.dataset.selectJoueur = "joueur"
            selection_bot.style.backgroundColor = 'green'
            selection_bot_case2 = div_bot[Number(select_bat) + 1] 
            selection_bot_case2.dataset.selectJoueur = "joueur"
            selection_bot_case2.style.backgroundColor = 'green'
            selection_bot_case3 = div_bot[Number(select_bat) + 2] 
            selection_bot_case3.dataset.selectJoueur = "joueur"
            selection_bot_case3.style.backgroundColor = 'green'
            selection_bot_case4 = div_bot[Number(select_bat) + 3] 
            selection_bot_case4.dataset.selectJoueur = "joueur"
            selection_bot_case4.style.backgroundColor = 'green'
            selection_bot_case5 = div_bot[Number(select_bat) + 4] 
            selection_bot_case5.dataset.selectJoueur = "joueur"
            selection_bot_case5.style.backgroundColor = 'green'


            
            option[5].innerHTML = ""
            option[5].value = ""
            
        }else if(hor.checked == false && ver.checked == true){

            
            select_bat = this.dataset.nbcase_bot - 1

            var selection_bot = div_bot[select_bat]
            selection_bot.dataset.selectJoueur = "joueur"
            selection_bot.style.backgroundColor = 'green'
            selection_bot_case2 = div_bot[Number(select_bat) + 10] 
            selection_bot_case2.dataset.selectJoueur = "joueur"
            selection_bot_case2.style.backgroundColor = 'green'
            selection_bot_case3 = div_bot[Number(select_bat) + 20] 
            selection_bot_case3.dataset.selectJoueur = "joueur"
            selection_bot_case3.style.backgroundColor = 'green'
            selection_bot_case4 = div_bot[Number(select_bat) + 30] 
            selection_bot_case4.dataset.selectJoueur = "joueur"
            selection_bot_case4.style.backgroundColor = 'green'
            selection_bot_case5 = div_bot[Number(select_bat) + 40] 
            selection_bot_case5.dataset.selectJoueur = "joueur"
            selection_bot_case5.style.backgroundColor = 'green'

            
            
            option[5].innerHTML = ""
            option[5].value = ""
        }else{
            console.error('BUG SELECT')
        }
    }else{
        console.error('ERROR TYPE')
    }


}


function verif(){

    if(this.dataset.selectBot == "bot"){

        this.style.backgroundColor = 'red';

    }else{

        this.style.backgroundColor = 'white';

    }
    attaque_bot()
}

function selection_bot(){

    // aléatoire sur la grille 

    var barque1 = Math.floor(Math.random() * Math.floor(80))
    var barque2 = Math.floor(Math.random() * Math.floor(101))
    var navire1 = Math.floor(Math.random() * Math.floor(80))
    var navire2 = Math.floor(Math.random() * Math.floor(101))
    var cuirasse = Math.floor(Math.random() * Math.floor(70))
    var porte_avion = Math.floor(Math.random() * Math.floor(101))

    var all = document.getElementsByClassName('case')

    if(barque1 > 90 ){
        barque1 = barque1 - 21 
        return
    }else if(barque2 > 90){
        barque2 = barque2 - 2 
        return

    }else if(navire1 > 90){
        navire1 = navire1 - 21 
        return

    }else if(navire2 > 90) {
        navire2 = navire2 - 2 
        return

    }else if(cuirasse > 90){
        cuirasse = cuirasse - 31 
        return

    }else if(porte_avion > 90){
        porte_avion = porte_avion - 4 
        return

    }

    if(barque1 == 0 ){
        barque1 = barque1 + 1 
        return
    }else if(barque2 == 0){
        barque2 = barque2 +1
        return

    }else if(navire1 == 0){
        navire1 = navire1 +1 
        return

    }else if(navire2 == 0) {
        navire2 = navire2 +1
        return

    }else if(cuirasse == 0){
        cuirasse = cuirasse +1 
        return

    }else if(porte_avion == 0){
        porte_avion = porte_avion  +1
        return

    }

    
    // Mise en place des bâteaux sur la grille
    

    selection_1 = all[barque1 - 1]
    selection_1.dataset.selectBot = "bot"
    selection_1_case2 = all[barque1 + 10].dataset.selectBot = "bot"

    
    selection_2 = all[barque2 - 1]
    selection_2.dataset.selectBot = "bot"
    selection_2_case2 = all[barque1 + 1].dataset.selectBot = "bot"
    
    selection_3 = all[navire1 - 1]
    selection_3.dataset.selectBot = "bot"
    selection_3_case2 = all[navire1 + 10].dataset.selectBot = "bot"
    selection_3_case3 = all[navire1 + 20].dataset.selectBot = "bot"
    
    selection_4 = all[navire2 - 1]
    selection_4.dataset.selectBot = "bot"
    selection_4_case2 = all[navire2 + 1].dataset.selectBot = "bot"
    selection_4_case3 = all[navire2 + 2].dataset.selectBot = "bot"
    
    selection_5 = all[cuirasse - 1]
    selection_5.dataset.selectBot = "bot"
    selection_5_case2 = all[cuirasse + 9].dataset.selectBot = "bot"
    selection_5_case3 = all[cuirasse + 19].dataset.selectBot = "bot"
    selection_5_case4 = all[cuirasse + 29].dataset.selectBot = "bot"
    
    selection_6 = all[porte_avion - 1]
    selection_6.dataset.selectBot = "bot"
    selection_6_case2 = all[porte_avion + 1].dataset.selectBot = "bot"
    selection_6_case3 = all[porte_avion + 2].dataset.selectBot = "bot"
    selection_6_case4 = all[porte_avion + 3].dataset.selectBot = "bot"
    selection_6_case5 = all[porte_avion + 4].dataset.selectBot = "bot"
    
    

}

setGame()