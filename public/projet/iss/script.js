//______________________________________________________________________________
// Lien google de l'api
// https://console.developers.google.com/apis/credentials?project=api-iss-296418
//_______________________________________________________________________________

const url = "http://api.open-notify.org/iss-now.json"


var actu = document.getElementById('actu')

let count = 30;
let map;

function position(){

    fetch(url, {method: 'get'}).then(response => response.json()).then(results => {

        var latitude = results.iss_position.latitude;
        var longitude = results.iss_position.longitude;


        const myLatLng = { lat: Number(latitude), lng: Number(longitude) };
        const map = new google.maps.Map(document.getElementById("map"), {
          zoom: 6,
          center: myLatLng,
        });
        new google.maps.Marker({
          position: myLatLng,
          map,
          title: "ISS",
        });
      
        count = 30
        return count
    }).catch(err => {
        console.error('error')
        console.error(err)
    })
}

setInterval(position, 30000)

setInterval(function(){ 
    count = count - 1;
    actu.innerHTML = "La page s'actualise toute les " + count + " secondes."
    return count
},1000)