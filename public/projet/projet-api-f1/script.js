const apiUrl = "http://ergast.com/api/f1/"
const format = ".json"

let url; 

let year = new Date()
let number_year = year.getFullYear() + 1

var saison_select = document.getElementById('saison_select')
saison_select.addEventListener('change', addcourse)
var button_voir = document.getElementById('button_voir').addEventListener('click', definURL)
var choix_select = document.getElementById('choix_select')
var course_select = document.getElementById('course_select')

var tableau = document.getElementById('tableau')


function addsaison(){
    for( var i = 1950; i < number_year; i++){
        var option = document.createElement('option')
        saison_select.appendChild(option)
        option.value = i 
        option.innerHTML = i
    }
    addcourse()
}


function addcourse(){

    let url = apiUrl + saison_select.value + format
    
    fetch(url, {method: 'get'}).then(response => response.json()).then(results => {
        
        course_select.innerHTML = ''

        var option = document.createElement('option')
        course_select.appendChild(option)
        option.value = ""
        option.innerHTML = 'Classement Général'

        for( var i = 0; i < results.MRData.total; i++){

            var recupround = results.MRData.RaceTable.Races[i].round
            var recupcircuit = results.MRData.RaceTable.Races[i].raceName

            var option = document.createElement('option')
            course_select.appendChild(option)
            option.value = recupround
            option.innerHTML = recupcircuit
        }

    }).catch(err => {
        console.log(err)
        console.error('error function addcourse')
    })
}

function definURL(){

    if(course_select.value == ""){

        let url =  apiUrl + saison_select.value + '/' + choix_select.value + format
        recupvalue(url)

    }else if(course_select != ""){

        let url =  apiUrl + saison_select.value + '/' + course_select.value + '/' + choix_select.value + format
        recupvalue(url)
        
    }else{
        console.error('bug URL')
    }
}


function recupvalue(url){
    
    fetch(url, {method: 'get'}).then(response => response.json()).then(results => {
    
        

        if(choix_select.value == 'driverStandings'){


            var classement_texte = [
                'Position',
                'Point',
                'Victoire',
                'Prénom',
                'Nom',
                'Nationalité',
                'Team',

            ]


            tableau.innerHTML = ""

            var thead = document.createElement('th')
            tableau.appendChild(thead)
            thead.innerHTML = 'Classement Pilotes'

            var tbody = document.createElement('tbody')
            tableau.appendChild(tbody)

            var tr = document.createElement('tr')
            tbody.appendChild(tr)

            for(l = 0; l < classement_texte.length; l++){
                var td = document.createElement('td')
                tr.appendChild(td)
                td.innerHTML = classement_texte[l]
                td.className = "head"
            }
            
            for(var a = 0 ; a < results.MRData.total; a++){
                
                var tab = [
                    results.MRData.StandingsTable.StandingsLists[0].DriverStandings[a].position,
                    results.MRData.StandingsTable.StandingsLists[0].DriverStandings[a].points,
                    results.MRData.StandingsTable.StandingsLists[0].DriverStandings[a].wins,
                    results.MRData.StandingsTable.StandingsLists[0].DriverStandings[a].Driver.givenName,
                    results.MRData.StandingsTable.StandingsLists[0].DriverStandings[a].Driver.familyName,
                    results.MRData.StandingsTable.StandingsLists[0].DriverStandings[a].Driver.nationality,
                    results.MRData.StandingsTable.StandingsLists[0].DriverStandings[a].Constructors[0].name,
                ]
                
                
                var tr = document.createElement('tr')
                tbody.appendChild(tr)

                for(j = 0; j < tab.length; j++){
                    var td = document.createElement('td')
                    tr.appendChild(td)
                    td.innerHTML = tab[j]
                }
            }

        }else if(choix_select.value == 'constructorStandings'){


            var classement_const_texte = [
                'Position',
                'Point',
                'Victoire',
                'Team',
                'Nationalité',
            ]

            
            tableau.innerHTML = ""

            var thead = document.createElement('thead')
            tableau.appendChild(thead)
            thead.innerHTML = 'Classement Constructeurs'

            var tbody = document.createElement('tbody')
            tableau.appendChild(tbody)

            var tr = document.createElement('tr')
            tbody.appendChild(tr)

            for(l = 0; l < classement_const_texte.length; l++){
                var td = document.createElement('td')
                tr.appendChild(td)
                td.innerHTML = classement_const_texte[l]
                td.className = "head"
            }

            for(var a = 0 ; a < results.MRData.total; a++){

                var tab = [
                    results.MRData.StandingsTable.StandingsLists[0].ConstructorStandings[a].position,
                    results.MRData.StandingsTable.StandingsLists[0].ConstructorStandings[a].points,
                    results.MRData.StandingsTable.StandingsLists[0].ConstructorStandings[a].wins,
                    results.MRData.StandingsTable.StandingsLists[0].ConstructorStandings[a].Constructor.name,
                    results.MRData.StandingsTable.StandingsLists[0].ConstructorStandings[a].Constructor.nationality,
                ]

                var tr = document.createElement('tr')
                tbody.appendChild(tr)

                for(j = 0; j < tab.length; j++){
                    var td = document.createElement('td')
                    tr.appendChild(td)
                    td.innerHTML = tab[j]
                }
            }
            

        }else if(choix_select.value == 'qualifying'){

            var qualif_texte = [
                'Position',
                'Prénom',
                'Nom',
                'Team',
                'Q1',
                'Q2',
                'Q3',   
            ]

            tableau.innerHTML = ""

            var thead = document.createElement('thead')
            tableau.appendChild(thead)
            thead.innerHTML = 'Classement Qualification'

            var tbody = document.createElement('tbody')
            tableau.appendChild(tbody)

            var tr = document.createElement('tr')
            tbody.appendChild(tr)

            for(l = 0; l < qualif_texte.length; l++){
                var td = document.createElement('td')
                tr.appendChild(td)
                td.innerHTML = qualif_texte[l]
                td.className = "head"
            }

            for(var a = 0 ; a < results.MRData.total; a++){

                var tab = [
                    results.MRData.RaceTable.Races[0].QualifyingResults[a].position,
                    results.MRData.RaceTable.Races[0].QualifyingResults[a].Driver.givenName,
                    results.MRData.RaceTable.Races[0].QualifyingResults[a].Driver.familyName,
                    results.MRData.RaceTable.Races[0].QualifyingResults[a].Constructor.name,
                    results.MRData.RaceTable.Races[0].QualifyingResults[a].Q1,
                    results.MRData.RaceTable.Races[0].QualifyingResults[a].Q2,
                    results.MRData.RaceTable.Races[0].QualifyingResults[a].Q3,
                ]

                var tr = document.createElement('tr')
                tbody.appendChild(tr)

                for(j = 0; j < tab.length; j++){
                    var td = document.createElement('td')
                    tr.appendChild(td)
                    td.innerHTML = tab[j]
                }
            }

        }else if(choix_select.value == 'results'){
            

            var results_texte = [
                'Position',
                'Prénom',
                'Nom',
                'Team'
            ]

            tableau.innerHTML = ""

            var thead = document.createElement('thead')
            tableau.appendChild(thead)
            thead.innerHTML = 'Classement de la course'

            var tbody = document.createElement('tbody')
            tableau.appendChild(tbody)

            var tr = document.createElement('tr')
            tbody.appendChild(tr)

            for(l = 0; l < results_texte.length; l++){
                var td = document.createElement('td')
                tr.appendChild(td)
                td.innerHTML = results_texte[l]
                td.className = "head"
            }

            for(var a = 0 ; a < results.MRData.total; a++){


                var tab = [
                    results.MRData.RaceTable.Races[0].Results[a].position,
                    results.MRData.RaceTable.Races[0].Results[a].Driver.givenName,
                    results.MRData.RaceTable.Races[0].Results[a].Driver.familyName,
                    results.MRData.RaceTable.Races[0].Results[a].Constructor.name,
                    // results.MRData.RaceTable.Races[0].Results[a].Time.time,
                    // results.MRData.RaceTable.Races[0].Results[a].FastestLap.Time.time,
                ]

                var tr = document.createElement('tr')
                tbody.appendChild(tr)

                for(j = 0; j < tab.length; j++){
                    var td = document.createElement('td')
                    tr.appendChild(td)
                    td.innerHTML = tab[j]
                }
            }

        }
    }).catch(err => {
        console.log(err)
        console.error('error')
    })
}

addsaison() 


// http://ergast.com/api/f1/

// doc https://documenter.getpostman.com/view/11586746/SztEa7bL#46c7fbee-e90f-409f-b2ff-d8b77e85e5f6

// permet de recup tout les circuit de la saison http://ergast.com/api/f1/(year)

// permet d'avoir le classement par course http://ergast.com/api/f1/(year)/(round)/driverStandings

// permet d'avoir le classement constructeur http://ergast.com/api/f1/2020/1/constructorStandings.json